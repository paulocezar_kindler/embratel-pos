var Global = {
	initSliders : function () {
		var slideVids = null;
		var slideOlimpians = null;
		var screenWidth = jQuery(window).width();
		if( screenWidth < 991
			&& slideVids == null 
			&& slideOlimpians == null ) {
			jQuery('.list-videos').removeClass('grid-videos');
			slideVids = jQuery('.list-videos').bxSlider({
				auto: false,
				pager: false
			});
			slideOlimpians = jQuery('.delivery-slide').bxSlider({
				auto: false,
				pager: false
			});
		} else if (screenWidth > 991
			&& slideVids != null
			&& slideOlimpians != null ) {
			slideVids.destroySlider();
			slideOlimpians.destroySlider();
			jQuery('.list-videos').addClass('grid-videos');
			jQuery('.list-videos li').removeAttr('style');
			jQuery('.delivery-slide li').removeAttr('style');
			slideVids = null;
			slideOlimpians = null;
		}
	},
	resizeForm: function () {
		var screenWidth = jQuery(window).width();
		if( screenWidth > 991 ) {
			Global.equalheight('.eqBlock');
		} else {
			jQuery('.eqBlock').removeAttr('style');
		}
	},
	equalheight : function(container){
		var currentTallest = 0,
		currentRowStart = 0,
		rowDivs = new Array(),
			el,
			topPosition = 0;
		jQuery(container).each(function() {
			el = jQuery(this);
			jQuery(el).height('auto')
			topPostion = el.position().top;
			if (currentRowStart != topPostion) {
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
				}
				rowDivs.length = 0;
				currentRowStart = topPostion;
				currentTallest = el.height();
				rowDivs.push(el);
			} else {
				rowDivs.push(el);
				currentTallest = (currentTallest < el.height()) ? (el.height()) : (currentTallest);
			}
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
		});
	},
	fakecheck: function () {
		jQuery('#company .fake-check').on('click', function(e) {
			e.preventDefault();
			var _getThis = jQuery(this);
			if (_getThis.hasClass('active')){
				_getThis.removeClass('active');
				_getThis.prev('input:checkbox').attr('checked', false);
			} else	{
				_getThis.addClass('active');
				_getThis.prev('input:checkbox').attr('checked', true);
			}
		});
	},
	acordeon: function () {
		jQuery('#delivery .tab-area h4').on('click', function(e) {
			e.preventDefault();
			var _getThis = jQuery(this);
			jQuery('#delivery .tab-area').removeClass('active');
			_getThis.parent('.tab-area').addClass('active');
			$('html, body').animate({
				scrollTop: _getThis.offset().top - 100
			}, 1000);
		});
	},
	closemodal: function () {
		jQuery('.modal .closemodal').on('click', function(e) {
			e.preventDefault();
			jQuery('.modal').fadeOut();
        	jQuery('.modal iframe').attr('src', '');
		});		
	},
	openmodal: function () {
		jQuery('#olimpians .list-videos a').on('click', function(e) {
			e.preventDefault();
			var _getUrl = jQuery(this).attr('href');
			var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
		    var match = _getUrl.match(regExp);
		    if (match&&match[7].length==11) {
		    	jQuery('.modal').show();
		     	jQuery('.modal iframe').attr('src', 'https://www.youtube.com/embed/' + match[7] );
		    } 
		});
	},
	flaps : function () {
		jQuery('#delivery .tab-flaps a').on('click', function(e) {
			e.preventDefault();
			var _getThis = jQuery(this);
			var _getID = _getThis.attr('href');
			jQuery('.tab-group .tab-area').removeClass('active');
			jQuery('#delivery .tab-flaps li').removeClass('active');
			_getThis.parent('li').addClass('active');
			jQuery('.tab-group ' + _getID).addClass('active');
		});
	},
	openMenu : function () {
		jQuery('#header .openmenu').on('click', function(e) {
			e.preventDefault();
			var _getThis = jQuery(this);
			_getThis.toggleClass('active');
			jQuery('.menu').toggleClass('active');
		});

	},
	goToMenu : function () {
		jQuery('#header .nav a').on('click', function(e) {
			e.preventDefault();
			var _getID = jQuery(this).attr('href');
			jQuery('#header .openmenu').removeClass('active');
			jQuery('#header .menu').removeClass('active');
			$('html, body').animate({
				scrollTop: $(_getID).offset().top
			}, 1000);

		});
	},
	sendForm : function () {
		jQuery('#company form .submit').on('click', function(e) {
			e.preventDefault();
			console.log(jQuery('#company form').serialize());
		});
	},
	init : function () {
		this.resizeForm();
		this.fakecheck();
		this.initSliders();
		this.sendForm();
		this.goToMenu();
		this.openMenu();
		this.openmodal();
		this.closemodal();
		this.acordeon();
		this.flaps();
	}
}

jQuery(document).ready(function() {
	Global.init();
});

jQuery(window).on('resize',function(){
	Global.initSliders();
	Global.resizeForm();
	location.reload();
});