var ieHack = {
	centerContent : function () {
			
		jQuery('.content').each(function(index, el) {
			var _getWidth = jQuery(this).width();
			var _getHeight = jQuery(this).height();
			jQuery(this).css({
				'margin-left' : '-' + (_getWidth / 2) + 'px' ,
				'margin-top' : '-' + (_getHeight / 2) + 'px'
			});

		});
	},
	vhWebdoor : function () {
		var _wWidth = jQuery(window).width();
		var _wHeight = jQuery(window).height();
		var _this = jQuery('#webdoor, #header')
		if (_wWidth > 992) {
			_this.height(_wHeight);
		} else {
			_this.css('height','auto');
		}
	},
	init : function () {
		this.centerContent();
		this.vhWebdoor();
	}
}

jQuery(document).ready(function() {
	ieHack.init();
});

jQuery(window).load(function() {
	ieHack.init();
});

jQuery(window).resize(function(event) {
	ieHack.init();
});